package demoGroupId;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class WindowHandler {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/test/java/demoGroupId/drivers2/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Actions actions = new Actions(driver);

        /*
        Test case 4 - Window Handler case - 1 -------------------------------------
         */
        driver.get("http://automationpractice.com/index.php");
        WebElement newsletter = driver.findElement(By.xpath("//h4[contains(text(),'Newsletter')]"));
        js.executeScript("arguments[0].scrollIntoView();", newsletter);
        WebElement ele = driver.findElement(By.xpath("//li[@class='twitter']"));
        ele.click();
        Set<String> window = driver.getWindowHandles();
        Iterator<String> iter = window.iterator();
        String mainWindow = iter.next();
        String facebookWindow = iter.next();
        driver.switchTo().window(facebookWindow);
        driver.findElement(By.xpath("//span[contains(text(),'Join Group')]"));
        driver.switchTo().window(mainWindow);

        /*
        Window Handler case - 2 --------------------------------------
         */
        driver.get("http://qaclickacademy.com/practice.php");
        System.out.println(driver.findElements(By.tagName("a")).size());
        WebElement footerdriver = driver.findElement(By.id("gf-BIG"));// Limiting webdriver scope
        System.out.println(footerdriver.findElements(By.tagName("a")).size());
        //3-
        WebElement coloumndriver = footerdriver.findElement(By.xpath("//table/tbody/tr/td[1]/ul"));
        System.out.println(coloumndriver.findElements(By.tagName("a")).size());
        //4- click on each link in the coloumn and check if the pages are opening-
        for (int i = 1; i < coloumndriver.findElements(By.tagName("a")).size(); i++) {
            String clickonlinkTab = Keys.chord(Keys.CONTROL, Keys.ENTER);
            coloumndriver.findElements(By.tagName("a")).get(i).sendKeys(clickonlinkTab);
            Thread.sleep(5000L);
        }// opens all the tabs
        Set<String> windowHandles = driver.getWindowHandles();
        Iterator<String> it = windowHandles.iterator();
        while (it.hasNext()) {
            driver.switchTo().window(it.next());
            System.out.println(driver.getTitle());
        }

        //   ==============================================================
    /*    driver.get("http://automationpractice.com/index.php");
        WebElement newsletter = driver.findElement(By.xpath("//h4[contains(text(),'Newsletter')]"));
        js.executeScript("arguments[0].scrollIntoView();", newsletter);

        WebElement columnElement = driver.findElement(By.id("block_various_links_footer"));
        List<WebElement> list=columnElement.findElements(By.tagName("li"));
        for(int i=0;i<list.size();i++){
            WebDriverWait wait = new WebDriverWait(driver,10);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("li")));
            String clickonlinkTab=Keys.chord(Keys.CONTROL,Keys.ENTER);
            columnElement.findElements(By.tagName("li")).get(i).sendKeys(clickonlinkTab);
            Thread.sleep(5000L);
        }
*/


    }
}
