package demoGroupId;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchFunctionality {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/test/java/demoGroupId/drivers2/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Actions actions = new Actions(driver);

        /*
        Test case 2 - Search - Verify search results are rendered for correct search content
         */
        driver.get("http://automationpractice.com/index.php");
        driver.findElement(By.id("search_query_top")).sendKeys("Chiffon");
        driver.findElement(By.name("submit_search")).click();
        List<WebElement> listOfAddToCartButtons=driver.findElements(By.xpath("//a[@title='Add to cart']"));
        System.out.println("this is the count of add to cart buttons :::"+listOfAddToCartButtons.size());
        if(listOfAddToCartButtons.size()>1){
            System.out.println("It is working fine");
        }else{
            Assert.fail();
        }
    }

    }
