package demoGroupId;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Calendar {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/test/java/demoGroupId/drivers2/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
       // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Actions actions = new Actions(driver);
        driver.get("https://www.path2usa.com/travel-companions");

        By calenderBtn=By.xpath("//*[@id='travel_date']");
        WebDriverWait wait2 = new WebDriverWait(driver, 10);
        wait2.until(ExpectedConditions.presenceOfElementLocated(calenderBtn));
        driver.findElement(calenderBtn).click();
        List<WebElement> days=driver.findElements(By.className("day"));
        for(int i=0;i<days.size();i++){
            String text=days.get(i).getText();
            if(text.equalsIgnoreCase("21")){
                days.get(i).click();
                break;
            }
            //this is test code
        }


    }
}
