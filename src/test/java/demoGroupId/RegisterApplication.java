package demoGroupId;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class RegisterApplication {

    public static void main(String[] args){
        System.setProperty("webdriver.chrome.driver", "src/test/java/demoGroupId/drivers2/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Actions actions = new Actions(driver);

        /*
        Test case 1 - Register in the application - Fill personal information - Login with the created account
         */
        driver.findElement(By.xpath("//a[contains(text(),'Sign in')]")).click();
        driver.findElement(By.xpath("//h3[contains(text(),'Create an account')]"));
        Date d = new Date();
        String formatted = new SimpleDateFormat("HHmmss").format(d);
        System.out.println("this is my current date time:" + formatted);
        driver.findElement(By.id("email_create")).sendKeys("bigbangautomation" + formatted + "@gmail.com");
        driver.findElement(By.xpath("//span/i[@class='icon-user left']")).click();
        driver.findElement(By.xpath("//h3[contains(text(),'Your personal information')]"));
        driver.findElement(By.xpath("//input[@type='radio' and @value='2']"));
        driver.findElement(By.xpath("//input[@id='email']")).clear();
        driver.findElement(By.xpath("//input[@id='email']")).sendKeys("bigbangautomation" + formatted + "@gmail.com");
        Select days = new Select(driver.findElement(By.id("days")));
        days.selectByValue("19");
        Select month = new Select(driver.findElement(By.id("months")));
        month.selectByValue("8");
        Select year = new Select(driver.findElement(By.id("years")));
        year.selectByValue("1992");
        driver.findElement(By.id("id_gender1")).click();
        driver.findElement(By.id("customer_firstname")).sendKeys("Gurjot");
        driver.findElement(By.id("customer_lastname")).sendKeys("Ahuja");
        driver.findElement(By.id("passwd")).sendKeys("bigbangautomation123");
        driver.findElement(By.id("address1")).sendKeys("testautomation");
        driver.findElement(By.id("city")).sendKeys("testcity");
        Select state = new Select(driver.findElement(By.id("id_state")));
        state.selectByValue("1");
        driver.findElement(By.id("postcode")).sendKeys("00000");
        driver.findElement(By.id("phone_mobile")).sendKeys("9780897697");
        driver.findElement(By.id("alias")).clear();
        driver.findElement(By.id("alias")).sendKeys("test alias");
        driver.findElement(By.id("submitAccount")).click();

    }
}
