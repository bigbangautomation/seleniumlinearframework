package demoGroupId;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class ItemCheckout {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/test/java/demoGroupId/drivers2/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Actions actions = new Actions(driver);

        /*
                Test case 3 - Item checkout
         */
        driver.get("http://automationpractice.com/index.php");
        driver.findElement(By.id("search_query_top")).sendKeys("Faded Short Sleeve T-shirts");
        driver.findElement(By.name("submit_search")).click();
        //Find element by link text and store in variable "Element"
        WebElement Element = driver.findElement(By.xpath("//a[@title='Faded Short Sleeve T-shirts']"));
        //This will scroll the page till the element is found
        js.executeScript("arguments[0].scrollIntoView();", Element);
        WebElement HoverElement = driver.findElement(By.xpath("//a[@title='Faded Short Sleeve T-shirts']"));
        actions.moveToElement(HoverElement).perform();
        WebElement AddToCart = driver.findElement(By.xpath("//a[@title='Add to cart']"));
        AddToCart.click();
        WebElement Checkout = driver.findElement(By.xpath("//a[@title='Proceed to checkout']"));
        Checkout.click();
        WebElement price=driver.findElement(By.id("total_price"));
        js.executeScript("arguments[0].scrollIntoView();", price);
        String priceTxt=price.getText().replace("$","");
        if( priceTxt.matches("^\\d+\\.\\d+") )
            System.out.println(price+"----is a decimal number");
        else
            System.out.println(price+"----is a not decimal number");

    }
}
